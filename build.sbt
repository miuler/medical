enablePlugins(ScalaJSPlugin)

name := "Medical"
scalaVersion := "2.13.1"

scalaJSUseMainModuleInitializer := true // This is an application with a main method

libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.8"
libraryDependencies += "in.nvilla" %%% "monadic-html" % "0.4.0"



