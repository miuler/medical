package medical.frontend

import medical.frontend.components.{SearchComponent, RegisterComponent}
import mhtml._
import org.scalajs.dom


object MainApp {

  val mainApp =
    <div  class="container center">
      <div>
        <h1>Busqueda de pacientes</h1>
      </div>
      <br/>
      <main>
        {SearchComponent.component}
        <br/><br/><br/>
        {RegisterComponent.component}
      </main>
    </div>

  def main(args: Array[String]): Unit = {
    println("Hola Mundo")
    mount(dom.document.body, mainApp)
    ()
  }
}
