package medical.frontend.components

object RegisterComponent {

  def component: xml.Node = {
    <div>
      <div>
        <h4>-- Or --</h4>
      </div>
      <div>
        <input type="button" class="col s4 offset-s4 waves-effect waves-light btn" value="Registrar"></input>
      </div>
    </div>
  }

}
