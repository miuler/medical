package medical.frontend.components

import mhtml.Var

import scalajs.js
import org.scalajs.dom

object SearchComponent {

  val patients = Var("")

//  def onSearchInput(e: dom.TextEvent): Unit = {
  def onSearchInput(e: js.Dynamic): Unit = {
    val name = e.target.value.asInstanceOf[String]
    println(s"data: ${name}")

    if (name.length > 5) {
      patients := s"name: $name"
    } else {
      patients := ""
    }
  }

  def component: xml.Node = {
    <div>
      <div class="input-field">
        <input placeholder="Nombre del paciente"
               type="text"
               class="validate"
               autocomplete="off"
               oninput={ onSearchInput _ } id="first_name" />
        <label for="first_name">Nombre del paciente</label>
      </div>
      <div>{patients}</div>
    </div>
  }
}
